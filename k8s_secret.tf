
resource "kubernetes_secret" "docker-cfg" {
  count = var.registry_server == "" ? 0 : 1
  metadata {
    name ="${var.service_name}-secret"
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.registry_server}": {
      "auth": "${base64encode("${var.registry_username}:${var.registry_password}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"
}
