
resource "kubernetes_deployment" "service" {
  depends_on = [kubernetes_secret.docker-cfg]
  metadata {
    name = var.service_name
    labels = {
      app = var.service_name
    }
  }
  spec {
    selector {
      match_labels = {
        app = var.service_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.service_name
        }
      }
      spec {
        container {
//          image = "nginx:1.7.8"
          image = var.gitlab_image_url
          name = var.service_name
          port {
            container_port = var.container_port
          }


        }
        image_pull_secrets {
          name= "${var.service_name}-secret"
        }
      }
    }
  }
}



resource "kubernetes_service" "nginx-service" {
  depends_on = [kubernetes_deployment.service]
  metadata {
    name      = "${var.service_name}-service"
  }
  spec {
    selector = {
      app = var.service_name
    }
    type = "NodePort"
    port {
      node_port   = var.node_port
      protocol = "TCP"
      port = var.container_port
      target_port = var.container_port
    }
  }
}

