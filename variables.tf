variable "gitlab_api_token" {
  description = ""
}
variable "gitlab_image_url" {
  description = ""
}
variable "registry_server" {
  default = ""
}
variable "registry_username" {
  default = ""
}
variable "registry_password" {
  default = ""
}
variable "service_name" {
  description = ""
}
variable "container_port" {
  description = ""
}
variable "node_port" {
  description = "The range of valid ports is 30000-32767"
}
